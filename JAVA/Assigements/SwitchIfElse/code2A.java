


class Remark{
	public static void main(String[] args){
		char grade ='A';
		switch(grade){
			case 'O':
				System.out.println(grade+" grade is Outstanding");
				break;
			case 'A':
				System.out.println(grade+" grade is Excellent");
				break;
			case 'B':
				System.out.println(grade+" grade is  Good");
				break;
			case 'C':
				System.out.println(grade+" grade is Average");
				break;
			default:
				System.out.println(grade+" grade is Poor");
		}
	}
}

