


import java.io.*;
class Array6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());
		char arr[] = new char[size];
		for(int i=0;i<arr.length;i++){
			System.out.print("Enter element : ");
			arr[i] =(char)br.read();
			br.skip(1);
		}
		for(int i=0;i<arr.length;i++){			
			System.out.println(arr[i]);
		}
	}
}
