


import java.io.*;
class Array5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			System.out.print("Enter element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]<size){
				System.out.println(arr[i]+" is less than 10");
			}
		}
	}
}
