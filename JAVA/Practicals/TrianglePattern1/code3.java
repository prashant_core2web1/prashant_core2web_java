


import java.io.*;
class Pattern3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int ch = i;
			for(int j =1;j<=i;j++){
				System.out.print((char)(ch+64)+" ");
				ch++;
			}	
			System.out.println();
		}
	}
}

