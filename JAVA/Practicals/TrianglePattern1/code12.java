




import java.io.*;
class Pattern12{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num= i+64;
			for(int j =1;j<=row-i+1;j++){
				if(row%2 == 0){
					if(num%2 == 0){
						System.out.print((char)num +" ");
					}else{
						System.out.print(num +" ");
					}
				}else{
					if(num%2 == 0){
						System.out.print(num +" ");
					}else{
						System.out.print((char)num +" ");
					}
				}
				num++;
			}
			System.out.println();
		}
	}
}






