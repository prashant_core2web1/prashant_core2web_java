



import java.io.*;
class Pattern5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter n. of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i =1;i<=row;i++){
			int num = 1;
			for(int j =1;j<=i;j++){
				System.out.print(num*i+"  ");
				num++;
			}
			System.out.println();
		}
	}
}
