


class WhileLoop{
   public static void main(String[] args){
       int num = 9367924;
       int rem;
       int sum =0;
       int product=1;
	 while(num>0){
	   rem=num%10;
	   if(rem%2==1){
	      sum=sum+rem;
	   }
	   else{
	     product=product*rem;
	   }
	   num/=10;
       }
	   System.out.println("Sum of odd digits: "+sum);
	   System.out.println("Product of even digits: "+product);
   }
}
