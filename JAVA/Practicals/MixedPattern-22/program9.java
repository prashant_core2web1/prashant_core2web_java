


import java.util.*;
class Pattern9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=row;j>i;j--){
				System.out.print("   ");
			}
			int num=row+i-1;
			for(int k=1;k<=2*i-1;k++){
				System.out.print(num+"  ");
				num--;
			}
			System.out.println();
		}
	}
}
