



import java.util.*;
class Pattern4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=1;j<i;j++){
				System.out.print("\t");
			}
			int ch=i+64;
			for(int k=1;k<=row-i+1;k++){
				System.out.print((char)ch++ +"\t");
			}
			System.out.println();
		}
	}
}
