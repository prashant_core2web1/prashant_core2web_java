



import java.util.*;
class Pattern10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=row;j>i;j--){
				System.out.print("\t");
			}
			for(int k=1;k<=2*i-1;k++){
				if(k<i){
					System.out.print(num+"\t");
					num--;
				}
				else{
					System.out.print(num+"\t");
					num++;
				}
			}
			System.out.println();
		}
	}
}
