



import java.util.*;
class Array8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		char arr[] = new char[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<size;i++){
			arr[i] = sc.next().charAt(0);
		}
		System.out.println("Before reverse : ");
		for(int i=0;i<size;i+=2){
	            System.out.print(arr[i]+"  ");
		}
		for(int i=0;i<size;i++){
			int temp = arr[i];
			arr[i] = arr[size-1-i];
			//arr[size-1-i] = temp;
		}
		System.out.println("After reverse : ");
		for(int i=0;i<size;i+=2){
		System.out.print(arr[i]+"  ");
		}
	}
}
