



import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr1[] = new int[size];
		int arr2[] = new int[size];
		for(int i=0;i<size;i++){
			System.out.print("Enter elements at index "+i+" of 1st array: ");
			arr1[i] = sc.nextInt();
			System.out.print("Enter elements at index "+i+" of 2nd array: ");
			arr2[i] = sc.nextInt();
		}
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				if(arr1[i]==arr2[j]){
					System.out.print(arr1[i]+",");
				}
			}
		}
	}
}
