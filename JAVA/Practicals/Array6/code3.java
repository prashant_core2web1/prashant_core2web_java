



import java.util.*;
class Array3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}
		System.out.print("Enter search key : ");
		int key = sc.nextInt();
		int count = 0;
		for(int i=0;i<size;i++){
			if(arr[i]==key){
				count++;
			}
		}
		if(count>2){
			for(int i=0;i<size;i++){
				if(arr[i]==key){
					arr[i]=key*key*key;
				}
				System.out.print(arr[i]+" ");
			}
		}
	}
}
