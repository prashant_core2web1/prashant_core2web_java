


import java.util.*;
class Array5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size1 : ");
		int size1 = sc.nextInt();
		int arr1[] = new int[size1];
		System.out.println("Enter array1 elements : ");
		for(int i=0;i<size1;i++){
			arr1[i] = sc.nextInt();
		}
		System.out.print("Enter array size2 : ");
		int size2 = sc.nextInt();
		int arr2[] = new int[size2];
		System.out.println("Enter array2 elements : ");
		for(int i=0;i<size2;i++){
			arr2[i] = sc.nextInt();
		}
		int size = size1 + size2;
		int arr[] = new int[size];
		int j=0;
		int k=0;
		for(int i=0;i<size;i++){
			if(i<size1){
				arr[i] = arr1[j];
				j++;
			}
			else{
				arr[i] = arr2[k];
				k++;
			}
			System.out.print(arr[i]+"  ");
		}
	}
}
