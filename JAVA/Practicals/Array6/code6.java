



import java.util.*;
class Array6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}
		System.out.print("Enter search_key : ");
		int search_key = sc.nextInt();
		int count = 0;
		for(int i=0;i<size;i++){
			if(arr[i]%search_key ==0){
				System.out.println("An elements multiple of "+search_key+" found at index "+i);
				count++;
			}
		}
		if(count==0){
			System.out.print("Elements not found");
		}
	}
}
