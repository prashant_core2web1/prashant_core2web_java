



import java.util.*;
class Array9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}
		int count=0;
		for(int i=0;i<size;i++){
			int temp = arr[i];
			int rev=0;
			int rem=0;
			while(temp>0){
				rem=temp%10;
				rev=rev*10+rem;
				temp/=10;
			}
			if(rev==arr[i]){
				count++;

			}
		}
		System.out.print("There are "+count+" pailndrome elements in array.");
	}
}
