



class WhileLoop{
      public static void main(String[] argss){
          int num=214367689;
	  int rem;
	  int count1=0;
	  int count2=0;
	  while(num>0){
	     rem=num%10;
	     if(rem%2==0){
	         count1++;
	     }
	     else{
	        count2++;
	     }
	     num/=10;
	  }
	  System.out.println("The Even Count is: "+count1);
	  System.out.println("The Odd Count is: "+count2);
      }
}
