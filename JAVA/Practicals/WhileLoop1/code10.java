





class WhileLoop{
     public static void main(String[] args){
        long num = 9307922405l;
	long sum=0;
	long rem;
	while(num>0){
	   rem=num%10;
	   sum=sum+rem;
	   num/=10;
	}
	 System.out.print("The sum of digits in 9307922405 is "+sum);
     }
}
