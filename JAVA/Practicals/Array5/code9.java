


import java.util.*;
class Array9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number : ");
		int num = sc.nextInt();
		int count=0;
		int rev = 0;
		int rem = 0;
		int temp = num;
		while(temp>0){
			rem=temp%10;
			rev = rev*10+rem;
			count++;
			temp/=10;
		}
		int i=0;
		int arr[] = new int[count];
		int rem_2 = 0;
		while(rev>0){
			rem_2 = rev%10;
			arr[i] = rem_2 + 1;
			i++;
			rev/=10;
		}
		System.out.print(arr[i]+",");
	}
}
