

import java.util.*;
class Array5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.print("Enter Array Elements : ");
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}
		for(int i=0;i<size;i++){
			int temp= arr[i];
			int count =0;
			while(temp>0){
				count++;
				temp=temp/10;
			}
			System.out.print(count+",");
		}
	}
}
