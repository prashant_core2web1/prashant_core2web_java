


import java.util.*;
class Array10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("The factorials are : ");
		for(int i=0;i<arr.length;i++){
			int fact=1;
			int j=1;
			while(j<=arr[i]){
				fact = fact*j;
				j++;
			}
			System.out.print(fact+",");
		}
	}
}
