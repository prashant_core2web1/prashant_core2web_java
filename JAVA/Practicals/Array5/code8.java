
import java.io.*;
class Array8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int min = arr[0];
		for(int i=0;i<arr.length;i++){
			if(min>arr[i]){
				min=arr[i];
			}
		}
		int min_1 = arr[0];
		for(int i=0;i<arr.length;i++){
			if(min_1 > arr[i] && arr[i] > min){
				min_1 = arr[i];
			}
		}
		System.out.print(" The second minimum element in array is : "+min_1);
	}
}
