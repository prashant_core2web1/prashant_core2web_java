


import java.util.*;
class Array7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}
		for(int i=0;i<arr.length;i++){
			int j=1 , count=0;
			while(j<=arr[i]){
				if(arr[i]%j==0){
					count++;
				}
				j++;
			}
			if(count>2){
				System.out.println("Composite numbers in array are : "+arr[i]);
			}	
		}
	}
}
