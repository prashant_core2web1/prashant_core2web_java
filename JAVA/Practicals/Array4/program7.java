



import java.io.*;
class Array7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		char arr[] = new char[size];
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = (char)br.read();
			br.skip(1);
		}
		int temp=0;
		for(int i=0;i<arr.length;i++){
			temp=arr[i];
			if(temp>=97 && temp<=122){
				temp = temp-32;
				System.out.print((char)temp+" ");
			}
			else{
				System.out.print(arr[i]+" ");
			}
		}
	}
}
