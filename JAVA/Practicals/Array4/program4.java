




import java.io.*;
class Array4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.print("Enter the number to check : ");
		int check =Integer.parseInt(br.readLine());
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i] == check){
				count++;
			}
		}
		if(count>=2){
			System.out.println(check+" occures more than 2 times.");
		}
		else{
			System.out.println(check+" occuers less than 2 times.");
		}
	}
}
