



import java.io.*;
class Array6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		char arr[] = new char[size];
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] =(char)br.read();
			br.skip(1);
		}
		int count1 =0;
		int count2 =0;
		for(int i=0;i<arr.length;i++){
		  if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'||arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
			  count1++;
		  }
		  else{
			  count2++;
		  }
		}
		System.out.print("Count of vowels is : "+count1+"\n");
		System.out.print("Count of consonnts is : "+count2);
	}
}
