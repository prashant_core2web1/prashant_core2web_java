



import java.io.*;
class Array3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int max = arr[0];
		for(int i=0;i<arr.length;i++){
			if(max<arr[i]){
				max=arr[i];
			}
		}
		int max_1 = arr[0];
		for(int i=0;i<arr.length;i++){
			if(max_1 < arr[i] && arr[i] < max){
				max_1 = arr[i];
			}
		}
		System.out.print("Second largest element : "+max_1);
	}
}
