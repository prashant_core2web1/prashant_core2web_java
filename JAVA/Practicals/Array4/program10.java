


import java.util.*;
class Array10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Array Size: ");
		int size = sc.nextInt();
		char arr[] = new char[size];
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next().charAt(0);
		}
		System.out.print("Enter character key : ");
		char key = sc.next().charAt(0);
		for(int i=0;i<arr.length;i++){
			if(arr[i]==key){
				break;
			}
			System.out.println(arr[i]);
		}
	}
}
