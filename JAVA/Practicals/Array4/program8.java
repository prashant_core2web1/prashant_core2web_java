



import java.util.*;
class Array8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.next().charAt(0);
		}
		System.out.print("Enter the character to check : ");
		char check = sc.next().charAt(0);
		int count = 0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==check){
				count++;
			}
		}
		System.out.print(check+" occures "+count+" times in array.");
	}
}
