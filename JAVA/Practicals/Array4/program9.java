


import java.util.*;
class Array9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();
	        char arr[] = new char[size];
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.next().charAt(0);
		}
		System.out.println("Array is: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]>='a' && arr[i]<='z'){
				System.out.println(arr[i]);
			}
			else{
				arr[i]='#';
				System.out.println(arr[i]+"\t");
			}
		}
	}
}

