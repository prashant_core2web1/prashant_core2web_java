



import java.io.*;
class Array5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int temp;
		for(int i=0;i<arr.length;i++){
			temp = arr[i];
			arr[i] = arr[size-i-1];
			arr[size-i-1] = temp;
		}
		System.out.println("Reversed Array is : ");
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[size-i-1]+" ");
		}
	}
}
