


import java.util.*;
class Pattern2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		int num=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				System.out.print((char)(row+64)+""+num+" ");
				num--;
			}
			num+=(row + 1);
			System.out.println();
		}
	}
}
