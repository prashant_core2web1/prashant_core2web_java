



import java.util.*;
class Pattern5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=i;
			for(int j=1;j<=i;j++){
				System.out.print(num*j+" ");
			}
			System.out.println();
		}
	}
}
