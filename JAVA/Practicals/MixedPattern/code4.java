



import java.util.*;
class Pattern4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			int temp = row-i+1;
			for(int j=1;j<=i;j++){
				System.out.print(temp+" ");
				temp+=(row-i+1);
			}
			System.out.println();
		}
	}
}
