


import java.util.*;
class Pattern3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			int num = row+64;
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print((char)(num)+" ");
					num--;
				}
				else{
					System.out.print(j+" ");
				}
			}
			System.out.println();
		}
	}
}
