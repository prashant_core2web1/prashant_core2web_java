



import java.util.*;
class Pattern8{
	public static void main(String[] arrgs){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		int num = (row*(row+1))/2 ;
		for(int i=1;i<=row;i++){
			for(int j=row;j>=i;j--){
				System.out.print((char)(num+64)+"  ");
			        num--;
			}
			System.out.println();
		}
	}
}
