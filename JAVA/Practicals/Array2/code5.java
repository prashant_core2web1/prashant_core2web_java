

import java.util.*;
class Array5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int sum = 0;
		int arr[] = new int[size];
		System.out.println("Enter Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}
		for(int i=0;i<arr.length;i++){
			if(i%2==1){
			sum=sum+arr[i];
	        	}
		}
		System.out.print("Sum of odd indexed elements : "+sum);
	}
}
