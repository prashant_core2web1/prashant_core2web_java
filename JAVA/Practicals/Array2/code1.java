


import java.io.*;
class Array1{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size: ");
		int size = Integer.parseInt(br.readLine());
		int count = 0;
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			System.out.print("Enter array Elements: ");
			arr[i] = Integer.parseInt(br.readLine());
		}
	        System.out.print("Even Array Elements are:  ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
	                        System.out.print(arr[i]+"  ");
				count++;
			}
	
            }
	    System.out.println();
	System.out.println("Count of even array elements are: "+count);
	}
}

