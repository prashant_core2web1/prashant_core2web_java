

import java.util.*;
class Array9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int [size];
		for(int i=0;i<arr.length;i++){
			System.out.print("Array elements are : ");
			arr[i] = sc.nextInt();
		}
		int min = arr[0];
		for(int i=0;i<arr.length;i++){
			if(arr[i]<min){
				min = arr[i];
			}
		}
		System.out.print("Minimum number in the array is:  "+min);
	}
}
