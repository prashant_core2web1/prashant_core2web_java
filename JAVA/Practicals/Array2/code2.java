

import java.util.*;
class Array2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Array Size: ");
		int size = sc.nextInt();
		int sum = 0;
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			System.out.print("Array Elements are : ");
			arr[i] = sc.nextInt();
		}
	         System.out.print("Elements divisible by 3: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%3==0){
				System.out.print(arr[i]+"  ");
				sum+=arr[i];
			}
		}
		System.out.println();
		System.out.println("Sum of elements divisible by 3 is: "+sum);
	}
}
