


import java.util.*;
class Array6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int product = 1;
		int arr[] = new int [size];
		System.out.println("Enter array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();

			if(i %2 ==1){
				product = product * arr[i];
			}
		}
		System.out.print("Product of odd indexed elements are : "+product);
		
	}
}
