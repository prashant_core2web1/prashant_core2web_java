

import java.io.*;
class Array3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Array Size: ");
		int size = Integer.parseInt(br.readLine());
		char arr[] = new char[size];
		System.out.print("Enter Elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i] =(char)br.read();
			//br.skip(1);
	        }
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u' || arr[i]=='A' || arr[i]=='E' || arr[i]=='I' || arr[i]=='O' || arr[i]=='U'){
				System.out.println("Vowel "+ arr[i]+" found at index "+ i);
			}
               }
	}
}
