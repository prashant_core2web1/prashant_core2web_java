


import java.io.*;
class Pattern4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
			for(int i=1;i<=row;i++){
				int num = row + 64;
				for(int j=1;j<=i;j++){
					if(i%2 == 0){
						System.out.print((char)num+" ");
					}
					else{
						System.out.print((char)(num+32)+" ");
					}
					num--;
				}
				System.out.println();
			}
	}
}
