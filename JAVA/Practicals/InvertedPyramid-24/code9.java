



import java.util.*;
class Pattern9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=1;j<i;j++){
				System.out.print("\t");
			}
			for(int k=1;k<=(2*(row-i+1)-1);k++){
				if(k%2==1){
					System.out.print("1"+"\t");
				}
				else{
					System.out.print("0"+"\t");
				}
			}
			System.out.println();
		}
	}
}
