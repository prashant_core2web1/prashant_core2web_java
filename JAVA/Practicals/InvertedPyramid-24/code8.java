



import java.util.*;
class Pattern8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=1;j<i;j++){
				System.out.print("\t");
			}
			int num=i-1+1;
			for(int k=1;k<=(2*(row-i+1)-1);k++){
				if(k<row-i+1){
					System.out.print(num+"\t");
					num++;
				}
				else{
					System.out.print(num+"\t");
					num--;
				}
			}
			System.out.println();
		}
	}
}
