



import java.util.*;
class Pattern7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=1;j<i;j++){
				System.out.print("  ");
			}
			int num = 1;
			for(int k=1;k<=(2*(row-i+1)-1);k++){
				if(k<row-i+1){
					System.out.print((char)(num+64)+" ");
					num++;
				}
				else{
					System.out.print((char)(num+64)+" ");
					num--;
				}
			}
			System.out.println();
		}
	}
}
