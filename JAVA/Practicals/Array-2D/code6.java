import java.util.*;
class Demo {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int row = sc.nextInt();
    int col = sc.nextInt();
    int arr[][] = new int[row][col];
    int sum_prim = 0;
    int sum_sec = 0;;
   System.out.println("Enter elements : ");
  for(int i =0;i<row;i++) {
    for(int j =0;j<col;j++) {
       arr[i][j] = sc.nextInt();
       if(i==j) {
         sum_prim += arr[i][j];
       }
       if(i+j==row-1) {
         sum_sec += arr[i][j];
       }
    }
  }
    System.out.print("Sum of primary diagonal elements is : "+sum_prim);

  }
}
