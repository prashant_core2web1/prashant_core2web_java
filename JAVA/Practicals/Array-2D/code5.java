

import java.io.*;
class Array5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of row : ");
		int row = Integer.parseInt(br.readLine());
		System.out.print("Enter no. of columns : ");
		int col = Integer.parseInt(br.readLine());
		int arr[][] = new int[row][col];
	        int sum[] = new int[col];
                System.out.println("Enter elements : ");
                for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}
                for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				sum[j] += arr[i][j];
			}
		}
               for(int i=0;i<row;i++){
		       System.out.println("Sum of col "+i+" is : "+sum[i]);
	       }		       
	}
}
