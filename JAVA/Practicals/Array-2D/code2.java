
import java.io.*;
class Pattern2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row =Integer.parseInt(br.readLine());
		System.out.print("Enter no.of columns: ");
		int col =Integer.parseInt(br.readLine());
		int arr[][] = new int[row][col];
		int sum =0;	
		System.out.println("Array elements are : ");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				sum=sum+arr[i][j];
			}
			System.out.println();
		}
	        System.out.print("sum : "+sum);		
	}
}
