



import java.util.*;
class Pattern10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			int num =row-i+1;
			for(int j=row;j>=i;j--){
				if(row%2==0){
					if(i%2==0){
						System.out.print((char)(num+96)+" ");
					}
					else{
						System.out.print((char)(num+64)+" ");
					}
				}
				else{
					if(i%2==1){
						System.out.print((char)(num+96)+" ");
					}
					else{
						System.out.print((char)(num+64)+" ");
					}
				}
				num--;
			}
			System.out.println();

		}
	}
}
