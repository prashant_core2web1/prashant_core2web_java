


import java.util.*;
class Pattern7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
		int num =row-i+1;
			for(int j=row;j>=i;j--){
				if(j%2==0){
					System.out.print(num+" ");
				}
				else{
					System.out.print((char)(num+96)+" ");
				}
				num--;
			}
			System.out.println();
		}
	}
}
