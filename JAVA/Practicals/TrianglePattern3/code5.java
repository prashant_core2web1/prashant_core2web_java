



import java.util.*;
class Pattern5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows: ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			int num =1;
			for(int j=1;j<=row-i+1;j++){
				if(i%2==1){
					System.out.print((char)(num+64)+" ");
				}
				else{
					System.out.print((char)(num+96)+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
