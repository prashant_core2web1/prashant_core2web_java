

import java.util.*;
class Array3{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.print("Enter key: ");
		int key = sc.nextInt();
		int arr[] = new int[]{2,5,2,7,8,9,2};
		int count =0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==key){
				count++;
			}
		}
		if(count==0){
			System.out.print(key+" is not found");
		}
		else{
			System.out.print(key+" occured "+count+" times in array");
		}
	}
}
