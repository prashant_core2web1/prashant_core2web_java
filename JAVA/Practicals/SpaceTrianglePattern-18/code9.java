



import java.io.*;
class Pattern9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of row : ");
		int row = Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			for(int j=1;j<i;j++){
				System.out.print(" "+"\t");
			}
			int num = row;
			for(int k=row;k>=i;k--){
				System.out.print((char)(num+64)+"\t");
				num--;
			}
			System.out.println();
		}
	}
}
