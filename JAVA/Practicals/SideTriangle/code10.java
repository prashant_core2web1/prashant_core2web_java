


import java.util.*;
class Pattern10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		int col = 0;
		for(int i=1;i<2*row;i++){
			if(i<=row){
				col = row-i;
			}
			else{
				col = i-row;
			}
			for(int j=1;j<=col;j++){
				System.out.print("\t");
			}
			if(i<=row){
				col=i;
			}
			else{
				col = 2*row-i;
			}
			int num=row-col+1;
			for(int k=1;k<=col;k++){
				System.out.print((char)(num+64)+"\t");
				num++;
			}
			System.out.println();
		}
	}
}
