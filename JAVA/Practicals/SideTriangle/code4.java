


import java.util.*;
class Pattern4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows: ");
		int row = sc.nextInt();
		int col = 0;
		for(int i=1;i<2*row;i++){
			if(i<=row){
				col = i;
			}
			else{
				col--;
			}
			int num =row-col+1;
			for(int j=1;j<=col;j++){
				System.out.print(num+"  ");
			}
			//num--;
			System.out.println();
		}
	}
}
