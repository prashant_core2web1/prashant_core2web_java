

import java.util.*;
class Pattern8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number : ");
		int num=sc.nextInt();
		int temp=num;
		int rem=0;
		while(num>0){
			rem=num%10;
			num/=10;
			if(rem == 0){
				System.out.print(temp+" is a Duck number");
				break;
			}
			else{
				System.out.print(temp+" is not a Duck number");
				break;
			}
		}
	}
}
