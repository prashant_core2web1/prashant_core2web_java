


import java.io.*;
class Pattern4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number: ");
		int num = Integer.parseInt(br.readLine());
		int sum=0;
		for(int i=1;i<num;i++){
			if(num%i==0){
				sum=sum+i;
			}
		}
		if(sum>num){
			System.out.print(num+" is a abundant number");
		}
		else{
			System.out.print(num+" is not a abundant number");
		}
	}
}
