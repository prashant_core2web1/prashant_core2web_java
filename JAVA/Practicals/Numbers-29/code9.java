


import java.io.*;
class Harshad_Niven{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number : ");
		int num = Integer.parseInt(br.readLine());
		int sum = 0;
		int temp = num;
		int rem = 0;
		while(temp>0){
			rem = temp % 10;
			sum += rem;
			temp /= 10;
		}
		if(num%sum==0){
			System.out.print(num+" is a Harshad/Niven number");
		}
		else{
			System.out.print(num+" is not a Harshad/Niven number");
		}
	}
}
