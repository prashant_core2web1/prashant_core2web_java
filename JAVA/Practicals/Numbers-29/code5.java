


import java.io.*;
class Pattern5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number : ");
		int num = Integer.parseInt(br.readLine());
		int rev=0;
		int rem=0;
		int var=0;
		int temp=num;
		int digit=0;
		int sq;
		while(num>0){
			num/=10;
			digit++;
		}
		sq = temp*temp;
		while(digit>0){
			rem=sq%10;
			var= rem+var*10;
			sq/=10;
			digit--;
		}
		while(var>0){
			rem=var%10;
			var/=10;
			rev=rem+rev*10;
		}
		if(rev==temp){
			System.out.print(temp+" is automorphic number");
		}
		else{
			System.out.print(temp+" is not a automorphic number");
		}
	}
}
