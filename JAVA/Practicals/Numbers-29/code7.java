


import java.io.*;
class HappyNumber{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number : ");
		int num =Integer.parseInt(br.readLine());
		int temp = num;
		if(num==0){
			System.out.println(num+" is not a happy number");
		}
		else{
			while(true){
				int rem = 0;
				int sum = 0;
				while(temp>0){
					rem = temp%10;
					sum = sum + rem*rem;
					temp /=10;
				}
				temp = sum;
				if(sum ==1){
					System.out.println(num+" is a Happy Number");
					break;
				}
				else if(sum<10){
					System.out.print(num+" is not a Happy Number");
					break;
				}
			}
		}
	}
}
