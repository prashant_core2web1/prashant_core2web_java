



import java.util.*;
class Pattern10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		int num=row*row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(j==i){
					System.out.print("$"+" ");
				}
				else{
					System.out.print(num*j+" ");
				}
				num--;
			}
			System.out.println();
		}
	}
}
