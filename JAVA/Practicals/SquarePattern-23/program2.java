



import java.util.*;
class Pattern2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		int num = row+96;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(j>=row-i+1){
					System.out.print((char)(num-32)+" ");
				}
				else{
					System.out.print((char)num+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
