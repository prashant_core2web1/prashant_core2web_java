


import java.io.*;
class Pattern8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		int sp =0;
		int col =0;
		int num = 0;
		for(int i=1;i<2*row;i++){
			if(i<=row){
				sp=row-i;
				col=2*i-1;
				num=i;
			}
			else{
				sp=i-row;
				col=col-2;
				num=num-2;
			}
			for(int j=1;j<=sp;j++){
				System.out.print("\t");
			}
			for(int k=1;k<=col;k++){
				if(i<=row){
					if(k<i){
						System.out.print((char)(num+64)+"\t");
						num--;
					}
					else{
						System.out.print((char)(num+64)+"\t");
						num++;
					}
				}
				else{
					if(k<2*row-i){
						System.out.print((char)(num+64)+"\t");
						num--;
					}
					else{
						System.out.print((char)(num+64)+"\t");
						num++;
					}
				}
			}
			System.out.println();
		}
	}
}
