

import java.io.*;
class Pattern7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row =Integer.parseInt(br.readLine());
		int sp = 0;
		int col = 0;
		for(int i=1;i<row*2;i++){
			if(i<=row){
				sp = row-i;
				col = i*2-1;
			}
			else{
				sp = i-row;
				col = col-2;
			}
			for(int j=1;j<=sp;j++){
				System.out.print("\t");
			}
			char ch='A';
			for(int k=1;k<=col;k++){
				if(i<=row){
					if(k<i){
						System.out.print(ch+"\t");
						ch++;
					}
					else{
						System.out.print(ch+"\t");
						ch--;
					}
				}
				else{
					if(k<2*row-i){
						System.out.print(ch+"\t");
						ch++;
					}
					else{
						System.out.print(ch+"\t");
						ch--;
					}
				}
			}
			System.out.println();
		}
	}
}
