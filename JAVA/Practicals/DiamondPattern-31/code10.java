

import java.util.*;
class Pattern10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		int sp=0;
		int col=0;
		int temp=0;
		for(int i=1;i<2*row;i++){
			if(i<=row){
				sp=row-i;
				col=2*i-1;
				temp=1;
			}
			else{
				sp=i-row;
				col=col-2;
				temp=1;
			}
			for(int j=1;j<=sp;j++){
				System.out.print("\t");
			}
			for(int k=1;k<=col;k++){
				if(k<col/2+1){
					if(k%2==1){
						System.out.print(temp+"\t");
					}
					else{
						System.out.print((char)(temp+64)+"\t");
						temp++;
					}
				    }
				else{
					if(k%2==1){
						System.out.print(temp+"\t");
						temp--;
					}
					else{
						System.out.print((char)(temp+64)+"\t");
					}
				}
			}
			System.out.println();
		}
	}
}
