


import java.io.*;
class Pattern6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of row : ");
		int row = Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			for(int j=row;j>i;j--){
				System.out.print(" "+"\t");
			}
			int num=row;
			for(int k=1;k<=2*i-1;k++){
				if(k<i){
					System.out.print(num+"\t");
					num--;
				}
				else{
					System.out.print(num+"\t");
					num++;
				}
			}
			System.out.println();
		}
	}
}
