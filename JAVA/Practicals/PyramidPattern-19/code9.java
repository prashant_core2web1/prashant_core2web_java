


import java.util.*;
class Pattern9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=row;j>i;j--){
				System.out.print("\t");
			}
			int num=1;
			for(int k=1;k<=2*i-1;k++){
				if(i%2==1){
					if(k<i){
						System.out.print((char)(num+64)+"\t");
						num++;
					}
					else{
						System.out.print((char)(num+64)+"\t");
						num--;
					}
				}
				else{
					if(k<i){
						System.out.print((char)(num+96)+"\t");
						num++;
					}
					else{
						System.out.print((char)(num+96)+"\t");
						num--;
					}
				}
			}
			System.out.println();
		}
	}
}
