



import java.io.*;
class Pattern7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			for(int j=row;j>i;j--){
				System.out.print("\t");
			}
			int num =i;
			for(int k=1;k<=2*i-1;k++){
				if(i%2==1){
					System.out.print(num+"\t");
				}
				else{
					System.out.print((char)(num+64)+"\t");
				}
			}
			System.out.println();
		}
	}
}
