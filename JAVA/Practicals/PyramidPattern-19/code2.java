


import java.io.*;
class Pattern2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of row : ");
		int row = Integer.parseInt(br.readLine());
		int num = 1;
		for(int i=1;i<=row;i++){
			for(int j=row;j>i;j--){
				System.out.print("\t");
			}
			for(int k=1;k<=i*2-1;k++){
				System.out.print(num+"\t");
			     num++;
			}
			System.out.println();
		}
	}
}

