


import java.util.*;
class Pattern10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j= row;j>i;j--){
				System.out.print("\t");
			}
			int num=row-i+1;
			for(int k=1;k<=2*i-1;k++){
				if(k<i){
					System.out.print((char)(num+64)+"\t");
					num++;
				}
				else{
					System.out.print((char)(num+64)+"\t");
					num--;
				}
			}
			System.out.println();
		}
	}
}
