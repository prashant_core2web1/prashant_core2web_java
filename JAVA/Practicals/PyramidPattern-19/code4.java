



import java.util.*;
class Pattern4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of row : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=row;j>i;j--){
				System.out.print("\t");
			}
			int num=i;
			for(int k=1;k<=2*i-1;k++){
				System.out.print((char)(num+64)+"\t");
			}
			System.out.println();
		}
	}
}
