



import java.io.*;
class Palindrome{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number : ");
		int num = Integer.parseInt(br.readLine());
		int temp = num;
		int rem;
		int rev=0;
		while(temp>0){
			rem=temp%10;
			rev=rev*10+rem;
			temp/=10;
	}
	if(num==rev){
		System.out.println(num+" is a palindrome number.");
	}
	else{
		System.out.println(num+" is not a palindrome number.");
	}
     }
}
