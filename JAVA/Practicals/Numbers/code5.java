


import java.util.*;
class Reverse{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num : ");
		int num = sc.nextInt();
		int temp=num;
		int rem;
		int rev=0;
		while(temp>0){
			rem=temp%10;
			rev=rev*10+rem;
			temp/=10;
		}
		System.out.println("Reverse of "+num+" is "+rev);
	}
}
