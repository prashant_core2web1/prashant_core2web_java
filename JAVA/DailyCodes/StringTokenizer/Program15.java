


import java.util.*;
class SingleLine{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Player Info: ");
		String info = sc.nextLine();

		StringTokenizer st = new StringTokenizer(info," ");

		String str1 = st.nextToken();

		String str2 = st.nextToken();

		String str3 = st.nextToken();

		String str4 = st.nextToken();

		System.out.println("PlayerName : "+str1);

		System.out.println("JerNo : "+str2);

		System.out.println("Average : "+str3);

		System.out.println("Grade : "+str4);

	}	
}

//here error will be form when we change string Tokenizer value means we have taken the space but at the time of giving the user data that time we are using the ',' then it will give an error ...so always used the data or value which we have tahen in token....
