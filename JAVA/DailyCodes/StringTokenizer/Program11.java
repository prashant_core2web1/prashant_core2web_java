



import java.util.*;
class InputDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your name: ");
		String name = sc.next();

		System.out.print("Enter society name: ");
		String socName = sc.next();

		System.out.print("Enter wing: ");
		char wing = sc.next().charAt(0);

		System.out.print("Enter flatno : ");
		int flatNo = sc.nextInt();

		System.out.println("Name : "+name);
		System.out.println("Society Name : "+socName);
		System.out.println("wing : "+wing);
		System.out.println("FlatNo : "+flatNo);
	}
}

// here we are using the scanner class to take the input from the user....
// also most important thing is that here the character is converted using the syntax--"char wing = sc.next().charAt(0)".....
