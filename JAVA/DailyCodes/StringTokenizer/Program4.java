




import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{
	  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	  System.out.print("Enter your name : ");
	  String name = br.readLine();

	  System.out.print("Enter society name : ");
	  String socName = br.readLine();

	  System.out.print("Enter wing : ");
	  char wing = br.readLine();

	  System.out.print("Enter fltNo : ");
	  int flatNo = br.readLine();
	}
}


// here same the class(String) cannot converted into the primitive datatypes i.e."char","int"..both this are the primitive datatypes....
