




import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{
	  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	  System.out.print("Enter your name : ");
	  String name = br.readLine();

	  System.out.print("Enter society name : ");
	  String socName = br.readLine();

	  System.out.print("Enter wing : ");
	  char wing = br.readLine();

	  System.out.print("Enter fltNo : ");
	  int flatNo = Integer.parseInt(br.readLine());
	}
}


// here same the class(String) cannot converted into the primitive datatypes i.e."char" ...this are the primitive datatypes....

// here one new concept is introduce that is Rapper class ...which convert the string into primivite datatypes ...actually we are giving the data in the form of string but it internally converet this class into integer value ....this is the most important factor which we have to consider into all codes...
