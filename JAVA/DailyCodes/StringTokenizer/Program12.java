


// here we have take input from the user for th below code but in a single line only....


import java.util.*;
class SingleLine{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		String info = sc.next();
		System.out.println(info);
	}
}

//here the next() will print only the 1st character value..
