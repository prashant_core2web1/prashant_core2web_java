





import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{
	  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	  System.out.print("Enter your name : ");
	  String name = br.readLine();

	  System.out.print("Enter society name : ");
	  String socName = br.readLine();

	  System.out.print("Enter wing : ");
	  char wing  = (char)(br.read());

	  br.skip(1);

	  System.out.print("Enter flatNo : ");
	  String flatNo =br.readLine();

	  System.out.println("Name: "+name);
	  System.out.println("Society Name: "+socName);
	  System.out.println("Wing: "+wing);
	  System.out.println("FlatNo: "+flatNo);
	}
}

// here we know that the after the wing directly the flat no is printed blank space this is why because in char we only store th one character but in this condition the '\n' is also consider the one character and it is stored in the bufferReader but here the character take only one character from that is 'A' and the another character '\n' remains in buffered reader ..after we accessing the flatNo in that it will printing the '\n' ASCII value that is blank spcace.....here in buffered reader already the'\n' is present thats why it will not take thee value from user and directly printing the '\n'value...

//here to remove this '\n' from the bufferedreader we use the "br.skip(1)".This will remove the '\n' from the bufferedReader and we get the output whicg we want i.e...we get the flat no...
