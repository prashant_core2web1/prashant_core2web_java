


class UnaryOperator{
	public static void main(String[] args){
	       int x = 5;
       System.out.println(x++);  //actual value of x is printed here whic is 5.   
       System.out.println(x);  // the incremented value of x is printed here which is 6.
	}
}

/*
 Internal function call..
   line1---PostIncrement function...
     int postIncrement(x){
        int temp = x;
	x = x + 1; here are increment the value of x.
	return temp;  // actual value of x is printed which is 5.
     }
*/
