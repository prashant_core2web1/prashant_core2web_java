/*
Logical Operators:
1.&& -(Logical AND)
2.|| -(Logical OR)
3.! -(Logical NOT)
*/


class LogicalOperator{
	public static void main(String[] args){
		boolean x = true;
		boolean y= false;
	System.out.println(x && y);
	System.out.println(x || y);
	System.out.println(!x);
	}
}
