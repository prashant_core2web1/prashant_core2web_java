/*
Assignment Operators:
1.=
   shorthand
   1.+=
   2.-=
   3.*=
   4./=
   5.%=
*/

class AssignmentOperator{
	public static void main(String[] args){
		int x = 10;
		int y = 5;
	System.out.println(x);
	System.out.println(y);
	x += y;   // x = x + y
	System.out.println(x);
	System.out.println(y);
	}
}
