
// Quize problems.....

class QuizeDemo{
	public static void main(String[] args){
		byte b = 100;
		short s = 120;
		b -= s;      // b = b -s
		s += b;     // s = s + b
	System.out.println(b); // b=b-s = 100-120 = -20...here the value of b is update in the given box..that value became -20....
	System.out.println(s);  // s=s+b = 120-20 = 100....here updated value of b is taken that is -20...
	}
}
