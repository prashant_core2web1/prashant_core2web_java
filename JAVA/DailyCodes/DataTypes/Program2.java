/*
Unary Operators:
1.+
2.-
3.++
4.--
*/

class UnaryOperator{
	public static void main(String[] args){
		int x = 10;
           System.out.println(+x);
           System.out.println(-x);
           System.out.println(++x);
           System.out.println(--x);
        }
}

/*Internal function call.....
   line3-- PreIncrement function

   int preIncrement(x){  // value of x is 10..actual value...
	   x = x + 1;
	   return x;  //value of x becomes 11.
  }
  line4-- PreDecrement function 

  int preDecrement(x){ // value of x is 11...update value is taken as x...
          x = x -1;
	  return x;  //value of x becomes 10.
  }
*/
 




