// Leftshift operator: (<<)


class BitwiseOperator{
	public static void main(String[] args){
		int x = 5; // 00000101
	System.out.println(x<<2); // here in this operator we are shifting the biniary 2 bits towards leftside...
	}
}

/*
here if the 1st bit is 0 then the value which will be printed is positive....
here if the 1st bit is 1 then the value which will be printed is negative....
 e.g  5 biniary is 00000101
        here operation is x<<2
	which means that value will shifted 2 bits leftside..
	value will became 00010100 whic gives 20.
*/



