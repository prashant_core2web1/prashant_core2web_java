


class UnaryOperator{
	public static void main(String[] args){
		int x = 7;
	System.out.println(x--); // here same actual value of x is printed that is 7.
	System.out.println(x);  // here the decremented vaule of x is printed that is 6.
	}
}

/*
 Internal function call....
 line1----PostDecrement Function..
   int postDecrement(x){ 
       int temp = x;
       x = x -1; // the value of x is decremented by 1 ..and gives the value of x is 6.
       return temp; // actual value of x is printed that is 7
   }
*/
