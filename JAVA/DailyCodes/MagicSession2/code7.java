




import java.util.*;
class NestedFor{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of rows : ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=row;j>=1;j--){
				System.out.print((char)(j+64+32)+" ");
			}
			System.out.println();
		}
	}
}
