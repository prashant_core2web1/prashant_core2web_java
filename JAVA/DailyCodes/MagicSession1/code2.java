



import java.util.*;
class InputDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num : ");
		int num = sc.nextInt();
		if(num>=18){
			System.out.print("Voter is eligible for voting.");
		}
		else if(num<18 && num>=0){
			System.out.print("Voter is not eligible for voting.");
		}
		else{
			System.out.print("Invalid age/number is given.");
		}
	}
}

