


import java.util.*;
class InputDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num : ");
		int num = sc.nextInt();
		if(num%16==0){
			System.out.print(num+" "+" is present in table of 16.");
		}
		else{
			System.out.print(num+" "+" is not present in table of 16.");
		}
	}
}
