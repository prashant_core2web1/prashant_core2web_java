
class SwitchDemo{
	public static void main(String[] args){
		int num=2;
		System.out.println("Before Switch");
		switch(num){
                            case1 :
	             		System.out.println("One");
				break;
                            case2 :
	             		System.out.println("Two");
				break;
                            case3 :
	             		System.out.println("Three");
				break;
			    default :
                 		System.out.println("Invalid");
		}
		System.out.println("After Switch");
	}
}

