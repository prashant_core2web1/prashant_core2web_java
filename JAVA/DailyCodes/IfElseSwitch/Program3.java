



class IfElseDemoA{
	public static void main(String[] args){
		int age = 27;
		System.out.println("Start code");
		if(age>=18){
		System.out.println("Eligible for voting");
		}
		else{
		System.out.println("Not Eligible for voting");
		}
		System.out.println("End code");
	}
}


/*
 here we can see that there at a time only one statement is excuted.....like wise if statement is true then it will directly print the content which are present in if...otherwise it will directly go to the else statement...
 */
