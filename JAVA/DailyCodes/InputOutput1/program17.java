





import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr = new InputStreamReader(System.in);
		char x = isr.read();
		System.out.println(x);
	}
}


// As the return type of read() is int and we are trying to save the integer into character leading to error...
