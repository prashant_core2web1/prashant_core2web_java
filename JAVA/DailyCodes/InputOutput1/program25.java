



import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{ 
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter company name: ");
		String cmpName = br.readLine();

		System.out.print("Enter employee name: ");
		String empName = br.readLine();

		System.out.print("Enter empl id: ");
		int emplId =Integer.parseInt(br.readLine());

		System.out.println("Company Name: "+cmpName);
		System.out.println("Employee Name: "+empName);
		System.out.println("Employee ID: "+emplId);

	}
}

// Here any data we are giving as an input is always in string format and here comes the wrapper class which takes the string and parase it to our required type of data 
