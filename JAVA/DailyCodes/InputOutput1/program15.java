


import java.io.*;
class InputDemo{
	public static void main(String[] args){
		InputStreamReader isr = new InputStreamReader(System.in);
		int x = isr.read();
	}
}

/*
  Here read method throws the exception,which must be caught or declared to be thrown..
  when exception occur and are not handled it becomes the reason to collapse the code.
  *the read() only reads a single character.
*/
