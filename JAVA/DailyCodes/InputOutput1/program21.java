






import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr = new InputStreamReader(System.in); 

		BufferedReader br = new BufferedReader(isr);

		String name = br.readLine();
		System.out.println("Name:"+name);

		br.close();
		
		String compName = br.readLine();
		System.out.println("Company name: "+compName);
	}
}

// Here the exception is due to the line br.close() as this disconnects the connection between buffered reader and keyboard and after breaking the connection we are trying to take input from the user.
