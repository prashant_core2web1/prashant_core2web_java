// Here, even though the input is in string format because of readLine().When we are taking input for emplId we are using Wrapper class Integer to parse thestring into integer.



import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{ 
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter company name: ");
		String cmpName = br.readLine();

		System.out.print("Enter employee name: ");
		String empName = br.readLine();

		System.out.print("Enter empl id: ");
		int emplId =Integer.parseInt(br.readLine());

		System.out.print("Employee Salary: ");
	        double empSal = Double.parseDouble(br.readLine());

		System.out.println("Company Name: "+cmpName);
		System.out.println("Employee Name: "+empName);
		System.out.println("Employee ID: "+emplId);
		System.out.println("Employee Salary: "+""+"cr"+empSal);

	}
}
 
