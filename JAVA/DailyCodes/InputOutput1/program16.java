




import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr = new InputStreamReader(System.in);
		int x = isr.read();
		System.out.println(x);
	}
}


// here main thing is that the inputstreamreader only reads a single character and it will print there ASCII value..
// for example:
// Input: Prashant is given from this only 1st character will be taken and printing the ASCII value of that single character like that it will take the P character and print the ASCII value of P i.e(80).
