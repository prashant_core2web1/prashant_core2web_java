





import java.util.Scanner;

class InputDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		System.out.println(x);
	}
}

// InputMismatchException occured at runtime and is known as exception..
