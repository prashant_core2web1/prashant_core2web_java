



class InputDemo{
       void fun(){
	       System.out.println("In fun method");
       }
       static void run(){
		System.out.println("In run method");
       }
       public static void main(String[] args){
		System.out.println("In main method");
		run(); //here it can access easily because it is static method ...
       }
}


/*
 In the class input demo there are 3 methods out of 3 the 2 methods are static methods..and 1 is non static method.

 Here task of a JVM is to call main() and the flow of the code is driven according to the main().If there is nothing in main() then JVM just call the method and get run.

 As the method is static so that i can call the method with class name i.e.InputDemo.run() thi is also valid.

 As the main() is stsic hence it call other ststic method directly.
 */
