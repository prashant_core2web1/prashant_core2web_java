




class BreakDemo{
	public static void main(String[] args){
		for(int i=1;i<=40;i++){
			if(i%5==0 && i%7==0){
				break;
			}
			System.out.println(i);
		}
	}
}



/*
 Real time example......
    ethe aaapn ase mhanu shakto ki ek mulga ahes ani tyala tyacha frd chi note deyla jayache ahes ..suppose to tyacha gharapasun 5km lamb ahes tr aapaan aplya gharun nighalo tr jata jata aplyala apla frd 3km varti bhetala tr aapan tyala tyacha noteboook tithech denar... mg aapan ethe break condtion cha use karto kar ki tyach ghar 5km ahes but to aplyala 3km vartich bhetala tr aapan tithun ch mage janar..mhanje jr conditioon satisfy zali tr aapan tithech condition la brak karto...
    IN simple words ekkhadi condition ahes te to prnt ch chalnar jo prnt condition satisfied false nahi hot to prnt mhanje aapan ase pan mhanu shakto ki ek train ahes ani ti jr pune pasun nighali tr tichh last destanation hei nagpur ahses but maz destanation hei nagar ahes ....tr condition nagar prnt chalel jevha ti condition nagar madhe jayel theva ti lagech for loop la break karel ......kar ki maz destanation hei nagar hote...
*/

