
/*
While Loop:
  Syantx:
     
     initialization
     while(Condition)
     {
       //body
       increment/decrement
     }

 code Execution sequence:
 1.initialization
 2.condition
 3.body
 4.increment/decrement

*/


class Loop{
	public static void main(String[] args){
		int i = 1;
		while(i<=5){
			System.out.println("Hello Students...");
			i++;
		}
	}
}
